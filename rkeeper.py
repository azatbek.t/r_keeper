import sys
import requests
from envparse import env
from datetime import datetime
import xml.etree.ElementTree as ET
from ssl_ignore import no_ssl_verification

def send_request(body):
    url = env('RKEEPER_HOST')
    username = env('RKEEPER_USERNAME')
    password = env('RKEEPER_PASSWORD')
    auth=(username, password)
    headers= {
        'Content-Type': 'text/xml'
    }
    with no_ssl_verification():
        response = requests.post(url, body, auth=auth, headers=headers)
        return response.text

def getSysInfo():
    body = '<?xml version="1.0" encoding="windows-1251"?><RK7Query><RK7CMD CMD="GetSystemInfo"></RK7CMD></RK7Query>'
    response = send_request(body)
    result = ''
    try:
        result = ET.parse(response)
    except Exception as error:
        print(error)
    return result

def getOrder(orderGuids):
    xmlGuid = ''
    startTime = datetime.now()
    orders = []
    for guid in orderGuids:
        xmlGuid = f'<Order guid="${guid.orderGUID}"/>'
        body = f'<?xml version="1.0" encoding="windows-1251"?><RK7Query><RK7CMD CMD="GetOrder">${xmlGuid}</RK7CMD></RK7Query>'
        responseData = send_request(body)
        try:
            result = ET.parse(responseData)
            for order in result.RK7QueryResult.Order:
                orderObj = {}
                orderObj.visit = guid.visitGUID
                orderObj.GUID = order.guid
                orderObj.ident = order.orderIdent
                orderObj.name = order.orderName
                orderObj.lastVersion = order.version
                orderObj.openTime = datetime.strptime(order.openTime)
                ID = order.Table[0].id,
                name = order.Table[0].name
                orderObj.table = (ID, name)
                ID = order.Waiter[0].id
                name = order.Waiter[0].name
                orderObj.waiter = (ID, name)
                orderObj.dishes = []
                dishNumber = 1
                if order.Session:
                    continue
                for dish in order.Session:
                    if ('Dish' in dish):
                        for item in dish.Dish:
                            qnt = int(item.quantity) / 1000
                            if qnt < 1:
                                qnt = 1

                                dishObj = {}
        except Exception as error:
            print(error)
    endTime = datetime.now()
    print(endTime - startTime)
    return orders
    
def getVisits():
    info = getSysInfo()
    visitYear = info.RK7QueryResult.SystemInfo[0].ShiftDate[0, 4]
    visitMonth = info.RK7QueryResult.SystemInfo[0].ShiftDate[4, 6]
    visitDay = info.RK7QueryResult.SystemInfo[0].ShiftDate[6, 8]
    visitDate = datetime.strptime(f'${visitYear}-${visitMonth}-${visitDay}')
    restaurant = info.RK7QueryResult.SystemInfo[0].Restaurant[0].name
    body = '<?xml version="1.0" encoding="windows-1251"?><RK7Query><RK7CMD CMD="GetOrderList"></RK7CMD></RK7Query>'
    visits = []
    response = send_request(body)
    data = response.data
    result = ''
    try:
        result = ET.parse(data)
    except Exception as error:
        print(error)
    
    for visit in result.RK7QueryResult.Visit:
        visitObj = {}
        visitObj.GUID = visit.guid
        visitObj.finished = visit.Finished = 0 if visit.Finished else True
        visitObj.visitDate = visitDate
        visitObj.orders = []
        for order in visit.Orders:
            for orderInfo in order.Order:
                orderObj = {}
                orderObj.GUID = orderInfo.guid
                orderObj.lastVersion = orderInfo.Version
                visitObj.orders.push(orderObj)
        visits.push(visitObj)
    return visits

def getClassificators():
    body = '<?xml version="1.0" encoding="windows-1251"?><RK7Query><RK7CMD CMD="GetRefData" RefName="CLASSIFICATORGROUPS"/></RK7Query>'
    response = send_request(body)
    data = response
    result = ''
    try:
        result = ET.parse(data)
        return result
    except Exception as error:
        print(error)

func = getSysInfo()
print(func)

sys.modules[__name__] = getOrder, getVisits, getSysInfo, getClassificators